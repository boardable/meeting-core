"use strict";
// This file handles the base message passing of the meeting core packages.
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionPersistenceExtensionTypes = exports.SHARED_EVENTS = exports.MEETING_CORE_ID = exports.useMeetingCore = void 0;
const useMeetingCore_1 = require("./useMeetingCore");
Object.defineProperty(exports, "useMeetingCore", { enumerable: true, get: function () { return useMeetingCore_1.useMeetingCore; } });
exports.MEETING_CORE_ID = 'meeting-core';
var SHARED_EVENTS;
(function (SHARED_EVENTS) {
    SHARED_EVENTS["CORE_INFORMING_EXTENSION_OF_PERMISSIONS"] = "CORE_INFORMING_EXTENSION_OF_PERMISSIONS";
    SHARED_EVENTS["EXTENSION_IS_READY"] = "EXTENSION_IS_READY";
    SHARED_EVENTS["SHOW_TOASTER_MESSAGE"] = "SHOW_TOASTER_MESSAGE";
    SHARED_EVENTS["ANNOUNCE_EXTENSION_PERMISSIONS"] = "ANNOUNCE_EXTENSION_PERMISSIONS";
    SHARED_EVENTS["ANNOUNCE_ROLES"] = "ANNOUNCE_ROLES";
    SHARED_EVENTS["HYDRATE_PERMISSIONS"] = "HYDRATE_PERMISSIONS";
    SHARED_EVENTS["REQUEST_ROLES"] = "REQUEST_ROLES";
    SHARED_EVENTS["SEND_ROLES_TO_EXTENSION"] = "SEND_ROLES_TO_EXTENSION";
    SHARED_EVENTS["PERMISSIONS_UPDATE"] = "PERMISSIONS_UPDATE";
})(SHARED_EVENTS = exports.SHARED_EVENTS || (exports.SHARED_EVENTS = {}));
var PermissionPersistenceExtensionTypes;
(function (PermissionPersistenceExtensionTypes) {
    PermissionPersistenceExtensionTypes["UPDATE_PERMISSIONS_ON_SERVER"] = "UPDATE_PERMISSIONS_ON_SERVER";
    PermissionPersistenceExtensionTypes["UPDATE_LOCAL_PERMISSIONS"] = "UPDATE_LOCAL_PERMISSIONS";
    PermissionPersistenceExtensionTypes["HYDRATE_PERMISSIONS"] = "HYDRATE_PERMISSIONS";
})(PermissionPersistenceExtensionTypes = exports.PermissionPersistenceExtensionTypes || (exports.PermissionPersistenceExtensionTypes = {}));
class MeetingCorePackage {
    constructor() {
        this.onPassMessageToExtensions = function (msg) { console.log('Please replace me with a message dispatcher'); };
        this.handleRawMessage = (event) => {
            let data = undefined;
            let stringToParse = (event.nativeEvent && event.nativeEvent.data) || event.data;
            try {
                data = JSON.parse(stringToParse);
            }
            catch (e) {
                return;
            }
            data.local = data.local || false;
            data.sentFrom = 'boardable';
            this.onPassMessageToExtensions(data);
        };
    }
}
exports.default = MeetingCorePackage;
//# sourceMappingURL=index.js.map