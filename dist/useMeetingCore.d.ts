/// <reference types="react" />
import { ExtensionAction, SHARED_EVENTS, ExtensionType, ExtensionPermissionMap, MeetingRole } from './index';
declare function useMeetingCore({ sendMessageToExtension }: {
    sendMessageToExtension: any;
}): {
    handleMessage: (obj: ExtensionAction | {
        type: SHARED_EVENTS.CORE_INFORMING_EXTENSION_OF_PERMISSIONS;
        payload: {
            permissions: import(".").ExtensionScopedPermissions;
        };
        fromExtension: string;
        local?: boolean;
    } | {
        type: SHARED_EVENTS.EXTENSION_IS_READY;
        payload: {};
        fromExtension: string;
        local?: boolean;
    } | {
        type: SHARED_EVENTS.SHOW_TOASTER_MESSAGE;
        payload: import(".").ToastMessage;
        fromExtension: string;
        local?: boolean;
    } | {
        type: SHARED_EVENTS.HYDRATE_PERMISSIONS;
        payload: ExtensionPermissionMap;
        fromExtension: string;
        local?: boolean;
    } | {
        type: SHARED_EVENTS.ANNOUNCE_ROLES;
        payload: {
            roles: MeetingRole[];
        };
        fromExtension: string;
        local?: boolean;
    } | {
        type: SHARED_EVENTS.REQUEST_ROLES;
        payload: {};
        fromExtension: string;
        local?: boolean;
    } | {
        type: SHARED_EVENTS.ANNOUNCE_EXTENSION_PERMISSIONS;
        payload: {
            permissions: import(".").PermissionDeclaration;
        };
        fromExtension: string;
        local?: boolean;
    } | {
        type: SHARED_EVENTS.SEND_ROLES_TO_EXTENSION;
        payload: {
            roles: MeetingRole[];
        };
        fromExtension: string;
        local?: boolean;
    } | {
        type: SHARED_EVENTS.PERMISSIONS_UPDATE;
        payload: {
            permissions: import(".").Permission;
        };
        fromExtension: string;
        local?: boolean;
    }) => void;
    extensions: ExtensionType[];
    primaryExtension: string;
    setPrimaryExtension: import("react").Dispatch<import("react").SetStateAction<string>>;
    roles: MeetingRole[];
    currentUserRole: MeetingRole;
    permissionMap: {};
    toaster: {
        status?: 'success' | 'error';
        text?: string;
    };
    setExtensions: import("react").Dispatch<import("react").SetStateAction<ExtensionType[]>>;
    wrappedSetPermissionMap: (extension: ExtensionType) => (updatedMap: ExtensionPermissionMap) => void;
    selectedExtension: string;
    setSelectedExtension: import("react").Dispatch<import("react").SetStateAction<string>>;
};
export { useMeetingCore };
