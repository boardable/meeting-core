// This file handles the base message passing of the meeting core packages.

import { useMeetingCore } from './useMeetingCore';
export { useMeetingCore }

export const MEETING_CORE_ID = 'meeting-core';

export interface ExtensionAction {
    type: string, 
    payload: object,
    local?: boolean,
    fromExtension: string,
    sentFrom?: string, // way to track that Boardable sent the message
}

export enum SHARED_EVENTS {
	CORE_INFORMING_EXTENSION_OF_PERMISSIONS = 'CORE_INFORMING_EXTENSION_OF_PERMISSIONS',
	EXTENSION_IS_READY = 'EXTENSION_IS_READY',
	SHOW_TOASTER_MESSAGE = 'SHOW_TOASTER_MESSAGE',
	ANNOUNCE_EXTENSION_PERMISSIONS = 'ANNOUNCE_EXTENSION_PERMISSIONS',
	ANNOUNCE_ROLES = 'ANNOUNCE_ROLES',
	HYDRATE_PERMISSIONS = 'HYDRATE_PERMISSIONS',
	REQUEST_ROLES = 'REQUEST_ROLES',
	SEND_ROLES_TO_EXTENSION = 'SEND_ROLES_TO_EXTENSION',
	PERMISSIONS_UPDATE = 'PERMISSIONS_UPDATE',
}

export type MeetingRole = {
	id: number,
	name: string,
}

export type PermissionDeclaration = {
	[key: string]: {
		title: string,
		description: string,
	}
}

export type Permission = {
	[action: string]: boolean,
}

export type ExtensionScopedPermissions = {
	[roleId: string]: Permission,
}

export type ExtensionPermissionMap = {
    [extensionId: string]: ExtensionScopedPermissions
}

export type ToastMessage = {
	status: 'success' | 'error',
	text: string,
}

export type SharedEventAction = 
	{ type: SHARED_EVENTS.CORE_INFORMING_EXTENSION_OF_PERMISSIONS, payload: { permissions: ExtensionScopedPermissions }, fromExtension: string, local?: boolean} |
	{ type: SHARED_EVENTS.EXTENSION_IS_READY, payload: {}, fromExtension: string, local?: boolean} |
	{ type: SHARED_EVENTS.SHOW_TOASTER_MESSAGE, payload: ToastMessage , fromExtension: string, local?: boolean} |
	{ type: SHARED_EVENTS.HYDRATE_PERMISSIONS, payload: ExtensionPermissionMap , fromExtension: string, local?: boolean} |
	{ type: SHARED_EVENTS.ANNOUNCE_ROLES, payload: { roles: Array<MeetingRole> }, fromExtension: string, local?: boolean} |
	{ type: SHARED_EVENTS.REQUEST_ROLES, payload: {}, fromExtension: string, local?: boolean} |
	{ type: SHARED_EVENTS.ANNOUNCE_EXTENSION_PERMISSIONS, payload: { permissions: PermissionDeclaration } , fromExtension: string, local?: boolean} |
	{ type: SHARED_EVENTS.SEND_ROLES_TO_EXTENSION, payload: { roles: Array<MeetingRole> }, fromExtension: string, local?: boolean} |
	{ type: SHARED_EVENTS.PERMISSIONS_UPDATE, payload: { permissions: Permission }, fromExtension: string, local?: boolean}

export enum PermissionPersistenceExtensionTypes {
    UPDATE_PERMISSIONS_ON_SERVER = 'UPDATE_PERMISSIONS_ON_SERVER',
    UPDATE_LOCAL_PERMISSIONS = 'UPDATE_LOCAL_PERMISSIONS',
    HYDRATE_PERMISSIONS = 'HYDRATE_PERMISSIONS',
}

export type PermissionPersistenceActions = 
    { type: PermissionPersistenceExtensionTypes.UPDATE_PERMISSIONS_ON_SERVER, payload: { permissions: ExtensionPermissionMap }} |
    { type: PermissionPersistenceExtensionTypes.UPDATE_LOCAL_PERMISSIONS, payload: { permissions: ExtensionPermissionMap }} |
    { type: PermissionPersistenceExtensionTypes.HYDRATE_PERMISSIONS, payload: { permissions: ExtensionPermissionMap }};


type iframeOrWebViewEvent = {
	nativeEvent?: {
		data: string,
	},
	data?: string,
}

export type ExtensionType = {
	extension_id: string,
	title: string,
	url: string,
	hidden?: boolean,
	exclude_from_drawer?: boolean,
	permissionCapabilities?: ExtensionPermissionMap,
	font_awesome_class_name?: string,
};

export default class MeetingCorePackage {
	onPassMessageToExtensions = function(msg: any) { console.log('Please replace me with a message dispatcher'); };

	handleRawMessage = (event: iframeOrWebViewEvent) => {
		let data = undefined;

		let stringToParse = (event.nativeEvent && event.nativeEvent.data) || event.data;

		try {
			data = JSON.parse(stringToParse);
		} catch (e) {
			return;
		}

		data.local = data.local || false;
		data.sentFrom = 'boardable';

		this.onPassMessageToExtensions(data);
	}
}
