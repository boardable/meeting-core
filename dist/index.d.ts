import { useMeetingCore } from './useMeetingCore';
export { useMeetingCore };
export declare const MEETING_CORE_ID = "meeting-core";
export interface ExtensionAction {
    type: string;
    payload: object;
    local?: boolean;
    fromExtension: string;
    sentFrom?: string;
}
export declare enum SHARED_EVENTS {
    CORE_INFORMING_EXTENSION_OF_PERMISSIONS = "CORE_INFORMING_EXTENSION_OF_PERMISSIONS",
    EXTENSION_IS_READY = "EXTENSION_IS_READY",
    SHOW_TOASTER_MESSAGE = "SHOW_TOASTER_MESSAGE",
    ANNOUNCE_EXTENSION_PERMISSIONS = "ANNOUNCE_EXTENSION_PERMISSIONS",
    ANNOUNCE_ROLES = "ANNOUNCE_ROLES",
    HYDRATE_PERMISSIONS = "HYDRATE_PERMISSIONS",
    REQUEST_ROLES = "REQUEST_ROLES",
    SEND_ROLES_TO_EXTENSION = "SEND_ROLES_TO_EXTENSION",
    PERMISSIONS_UPDATE = "PERMISSIONS_UPDATE"
}
export declare type MeetingRole = {
    id: number;
    name: string;
};
export declare type PermissionDeclaration = {
    [key: string]: {
        title: string;
        description: string;
    };
};
export declare type Permission = {
    [action: string]: boolean;
};
export declare type ExtensionScopedPermissions = {
    [roleId: string]: Permission;
};
export declare type ExtensionPermissionMap = {
    [extensionId: string]: ExtensionScopedPermissions;
};
export declare type ToastMessage = {
    status: 'success' | 'error';
    text: string;
};
export declare type SharedEventAction = {
    type: SHARED_EVENTS.CORE_INFORMING_EXTENSION_OF_PERMISSIONS;
    payload: {
        permissions: ExtensionScopedPermissions;
    };
    fromExtension: string;
    local?: boolean;
} | {
    type: SHARED_EVENTS.EXTENSION_IS_READY;
    payload: {};
    fromExtension: string;
    local?: boolean;
} | {
    type: SHARED_EVENTS.SHOW_TOASTER_MESSAGE;
    payload: ToastMessage;
    fromExtension: string;
    local?: boolean;
} | {
    type: SHARED_EVENTS.HYDRATE_PERMISSIONS;
    payload: ExtensionPermissionMap;
    fromExtension: string;
    local?: boolean;
} | {
    type: SHARED_EVENTS.ANNOUNCE_ROLES;
    payload: {
        roles: Array<MeetingRole>;
    };
    fromExtension: string;
    local?: boolean;
} | {
    type: SHARED_EVENTS.REQUEST_ROLES;
    payload: {};
    fromExtension: string;
    local?: boolean;
} | {
    type: SHARED_EVENTS.ANNOUNCE_EXTENSION_PERMISSIONS;
    payload: {
        permissions: PermissionDeclaration;
    };
    fromExtension: string;
    local?: boolean;
} | {
    type: SHARED_EVENTS.SEND_ROLES_TO_EXTENSION;
    payload: {
        roles: Array<MeetingRole>;
    };
    fromExtension: string;
    local?: boolean;
} | {
    type: SHARED_EVENTS.PERMISSIONS_UPDATE;
    payload: {
        permissions: Permission;
    };
    fromExtension: string;
    local?: boolean;
};
export declare enum PermissionPersistenceExtensionTypes {
    UPDATE_PERMISSIONS_ON_SERVER = "UPDATE_PERMISSIONS_ON_SERVER",
    UPDATE_LOCAL_PERMISSIONS = "UPDATE_LOCAL_PERMISSIONS",
    HYDRATE_PERMISSIONS = "HYDRATE_PERMISSIONS"
}
export declare type PermissionPersistenceActions = {
    type: PermissionPersistenceExtensionTypes.UPDATE_PERMISSIONS_ON_SERVER;
    payload: {
        permissions: ExtensionPermissionMap;
    };
} | {
    type: PermissionPersistenceExtensionTypes.UPDATE_LOCAL_PERMISSIONS;
    payload: {
        permissions: ExtensionPermissionMap;
    };
} | {
    type: PermissionPersistenceExtensionTypes.HYDRATE_PERMISSIONS;
    payload: {
        permissions: ExtensionPermissionMap;
    };
};
declare type iframeOrWebViewEvent = {
    nativeEvent?: {
        data: string;
    };
    data?: string;
};
export declare type ExtensionType = {
    extension_id: string;
    title: string;
    url: string;
    hidden?: boolean;
    exclude_from_drawer?: boolean;
    permissionCapabilities?: ExtensionPermissionMap;
    font_awesome_class_name?: string;
};
export default class MeetingCorePackage {
    onPassMessageToExtensions: (msg: any) => void;
    handleRawMessage: (event: iframeOrWebViewEvent) => void;
}
