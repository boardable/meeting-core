"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useMeetingCore = void 0;
const react_1 = require("react");
const index_1 = require("./index");
function isSharedAction(obj) {
    return index_1.SHARED_EVENTS[obj.type] != undefined;
}
const handleMessage = ({ extensions, sendMessageToExtension, permissionMap, setToaster, setExtensions, setPermissionMap, currentUserRole, setRoles, roles }) => {
    return (obj) => {
        if (isSharedAction(obj)) {
            const extension = extensions.find(e => e.extension_id == obj.fromExtension);
            if (obj.type == index_1.SHARED_EVENTS.EXTENSION_IS_READY) {
                sendMessageToExtension(extension, {
                    fromExtension: index_1.MEETING_CORE_ID,
                    type: index_1.SHARED_EVENTS.CORE_INFORMING_EXTENSION_OF_PERMISSIONS,
                    payload: (permissionMap[extension.extension_id] && permissionMap[extension.extension_id][currentUserRole.id]) || {},
                });
            }
            else if (obj.type == index_1.SHARED_EVENTS.SHOW_TOASTER_MESSAGE) {
                setToaster({ status: obj.payload.status, text: obj.payload.text });
                setTimeout(() => {
                    setToaster({});
                }, 3000);
                return;
            }
            else if (obj.type == index_1.SHARED_EVENTS.ANNOUNCE_EXTENSION_PERMISSIONS) {
                const extensionIndex = extensions.findIndex(e => e.extension_id == obj.fromExtension);
                const extension = extensions[extensionIndex];
                extension.permissionCapabilities = obj.payload.permissions;
                let dupedExtensions = [...extensions];
                dupedExtensions.splice(extensionIndex, 1, extension);
                setExtensions([...dupedExtensions]);
                return;
            }
            else if (obj.type == index_1.SHARED_EVENTS.ANNOUNCE_ROLES) {
                setRoles(obj.payload.roles);
                extensions.forEach(extension => {
                    if (extension.extension_id == 'pusher' && obj.local) {
                        return;
                    }
                    sendMessageToExtension(extension, { type: index_1.SHARED_EVENTS.SEND_ROLES_TO_EXTENSION, payload: { roles: obj.payload.roles }, fromExtension: index_1.MEETING_CORE_ID });
                });
                return;
            }
            else if (obj.type === index_1.SHARED_EVENTS.REQUEST_ROLES) {
                // check every 500 ms if roles are populated and, if so, send them
                let sendRolesTimeout;
                const sendRoles = () => {
                    if (roles.length == 0) {
                        sendRolesTimeout = setTimeout(sendRoles, 500);
                    }
                    else {
                        sendMessageToExtension(extension, {
                            type: index_1.SHARED_EVENTS.SEND_ROLES_TO_EXTENSION,
                            payload: {
                                roles
                            },
                            fromExtension: index_1.MEETING_CORE_ID,
                        });
                        if (sendRolesTimeout) {
                            clearTimeout(sendRolesTimeout);
                        }
                        ;
                    }
                };
                sendRoles();
                return;
            }
            else if (obj.type === index_1.SHARED_EVENTS.HYDRATE_PERMISSIONS) {
                setPermissionMap(Object.assign({}, obj.payload.permissions));
                return;
            }
        }
        else {
            obj.sentFrom = 'boardable';
            extensions.forEach(extension => {
                if (extension.extension_id == 'pusher' && obj.local) {
                    return;
                }
                sendMessageToExtension(extension, obj);
            });
        }
    };
};
function useMeetingCore({ sendMessageToExtension }) {
    const [extensions, setExtensions] = react_1.useState([]);
    const [primaryExtension, setPrimaryExtension] = react_1.useState('video');
    const [roles, setRoles] = react_1.useState([]);
    const [currentUserRole, setCurrentUserRole] = react_1.useState({ id: 1, name: 'Test Meeting Member' }); // TODO: fetch from server
    const [permissionMap, setPermissionMap] = react_1.useState({});
    const [toaster, setToaster] = react_1.useState({});
    const [selectedExtension, setSelectedExtension] = react_1.useState(undefined);
    react_1.useEffect(() => {
        extensions.forEach(extension => {
            if (permissionMap[extension.extension_id]) {
                let permissions = permissionMap[extension.extension_id][currentUserRole.id] || {};
                sendMessageToExtension(extension, {
                    local: true,
                    fromExtension: index_1.MEETING_CORE_ID,
                    type: 'PERMISSIONS_UPDATE',
                    payload: { permissions }
                });
            }
        });
    }, [extensions, permissionMap, currentUserRole]);
    const wrappedSetPermissionMap = (extension) => {
        return (updatedMap) => {
            setPermissionMap(Object.assign({}, updatedMap));
            sendMessageToExtension(extension, { fromExtension: index_1.MEETING_CORE_ID, type: 'PERMISSIONS_UPDATE', payload: { permissions: (updatedMap[extension.extension_id][currentUserRole.id] || {}) } });
            const permissionsExtension = extensions.find(e => e.extension_id == 'permission-persistence');
            sendMessageToExtension(permissionsExtension, { fromExtension: index_1.MEETING_CORE_ID, type: index_1.PermissionPersistenceExtensionTypes.UPDATE_PERMISSIONS_ON_SERVER, payload: updatedMap });
            // send to other clients as well
            let pusher = extensions.find(e => e.extension_id == 'pusher');
            sendMessageToExtension(pusher, { fromExtension: index_1.MEETING_CORE_ID, type: index_1.SHARED_EVENTS.HYDRATE_PERMISSIONS, payload: { permissions: updatedMap } });
        };
    };
    // we build the handleMessage each time with the latest values passed in
    // it's weird, but it gets us around some issues with closures and keeps this
    // function easier to reason through since all of the message handling is in
    // another castle, er, function
    return {
        handleMessage: handleMessage({
            extensions,
            sendMessageToExtension,
            permissionMap,
            setToaster,
            setExtensions,
            setPermissionMap,
            currentUserRole,
            setRoles,
            roles
        }),
        extensions,
        primaryExtension,
        setPrimaryExtension,
        roles,
        currentUserRole,
        permissionMap,
        toaster,
        setExtensions,
        wrappedSetPermissionMap,
        selectedExtension,
        setSelectedExtension,
    };
}
exports.useMeetingCore = useMeetingCore;
//# sourceMappingURL=useMeetingCore.js.map